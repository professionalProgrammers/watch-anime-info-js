const watchAnime = require('./watchanime.info.js');
const https = require('http');
const fs = require('fs');
let urls = [
	// URLs of form "https://watchanime.info/episode/{episode}" go here
];

let series_urls = [
	// Series URLs of form "https://watchanime.info/{series}" go here
];

downloadSeries(series_urls[1]);
async function downloadSeries(url){
	let links = null;
	
	try{
		links = JSON.parse(fs.readFileSync('series-cache.json'))
	}catch(e){
		links = await watchAnime.getSeriesLinks(url);
	}
	
	console.log(links);
	
	fs.writeFileSync('series-cache.json', JSON.stringify(links));
	
	try{
		links = JSON.parse(fs.readFileSync('series-resolved-cache.json'));
	}catch(e){
		for(let i = 0; i != links.length; i++){
			links[i] = await getVideoLink(links[i]);
			console.log((i + 1) + ' video link(s) located');
		}
	}
	
	fs.writeFileSync('series-resolved-cache.json', JSON.stringify(links));
	
	console.log(links);
	for(let i = 0; i != links.length; i++){
		await download(links[i], i);
		console.log(i);
	}
}

function download(url, i){
	if(fs.existsSync('videos/' + i + '.mp4')) return;
	return new Promise(function(resolve, reject){
		https.get(url, function(response){
			https.get('http://stream.animestreams.net' + response.headers.location, function(response){
				console.log(response.statusCode);
				console.log(response.headers);
				
				response.pipe(fs.createWriteStream('videos/' + i + '.mp4'));
				response.on('end', function(){
					resolve();
				});
			});
		});
	});
}

async function getVideoLinks(arr){
	for(let i = 0; i != arr.length; i++){
		let link = await getVideoLink(arr[i]);
		console.log(link);
	}
}

async function getVideoLink(url){
	let videoData = await watchAnime.getVideoData(url);
	let link = await watchAnime.getVideoLink(videoData.filmId);
	return link;
}
