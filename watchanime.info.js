const https = require('https');
const htmlparser = require('htmlparser2');

module.exports.getVideoData = function(url){
	return new Promise((resolve, rej) => {
		https.get(url, (res) => {
			let body = '';
			res.on('data', function(data){
				body += data;
			});
			res.on('end', function(){
				let chunk_a_begin = body.indexOf('var filmId =');
				let chunk_a_length = 0;
				while(body[chunk_a_length + chunk_a_begin] !== '<'){
					chunk_a_length++;
				}
				let chunk_a = body.substr(chunk_a_begin, chunk_a_length);
				
				let chunk_b_begin = 0;
				let chunk_b_length = 1;
				
				while(chunk_a[chunk_b_begin] !== '"'){
					chunk_b_begin++;
				}
				chunk_b_begin++;
				
				while(chunk_a[chunk_b_begin + chunk_b_length]  !== '"'){
					chunk_b_length++;
				}
				
				let chunk_b = chunk_a.substr(chunk_b_begin, chunk_b_length);
				let result = Number(chunk_b);
				if(!result){
					return reject(result);
				}
				
				resolve({
					filmId: result
				});
			});
		});
	});
}

module.exports.getVideoLink = function(filmId, server = "vip"){
	return new Promise((resolve, reject) => {		
		https.get(`https://watchanime.info/ajax-get-link-stream/?server=${server}&filmId=${filmId}`, (res) => {
			let body = '';
			res.on('data', function(data){
				body += data;
			});
			
			res.on('end', () => {
				resolve(body);
			});
		});
	});
}

function convertToSecure(url){
	if(!url.startsWith('https')){
		return 'https' + url.substr(4);
	}
	return url;
}

module.exports.getSeriesLinks = function(url){
	let videoList = [];
	
	let inListEpisodes = false;
	let parser = new htmlparser.Parser({
		onopentag: function(name, attribs){
			if(!inListEpisodes && attribs.class === 'list-episodes') inListEpisodes = true;
			if(inListEpisodes && name === 'a'){
				videoList.push(convertToSecure(attribs.href));
			}
		},
		onclosetag: function(tagname){
			if(inListEpisodes && tagname === 'div'){
				inListEpisodes = false;
			}
		}
	});
	return new Promise(function(resolve, reject){
		https.get(url, function(response){
			let body = '';
			response.on('data', function(data){
				body += data;
			});
		
			response.on('end', function(){
				parser.write(body);
				resolve(videoList);
			});
		});
	});
}

module.exports.convertToSecure = convertToSecure;